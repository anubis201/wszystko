import { Component, OnInit } from '@angular/core';
import { HubConnection, HubConnectionBuilder, HttpTransportType } from '@aspnet/signalr';
import { messageShow, messageSend } from '../app/models/message';
import { format } from 'date-fns'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  title = 'chat';
  public hubConnection!: HubConnection;
  allMessages: messageShow[] = [];
  writer: string = '';
  constructor() { }

  ngOnInit() {
    this.hubConnection = new HubConnectionBuilder()
      .withUrl("https://localhost:5001/chathub", {
        transport: HttpTransportType.WebSockets,
        skipNegotiation: true
      })
      .build();

      this.hubConnection.on("ReceiveMessage", (user, message) => {
        const data: messageShow  = {
          nick: user,
          content: message,
          date: format(new Date(), 'MMM dd, y, HH:mm:ss')
        };
        this.allMessages.push(data)
    });

    this.hubConnection.start().then((data) => {
      console.log('gotowe')
    }).catch(err => console.error(err));
  }
  changeWriter(date: string):void {
    this.writer = date;
  }
  onSendMessage(message: messageSend) {
    // proste this.hubConnection.invoke("SendMessage", message.nick, message.content).catch(err => console.error(err));
    this.hubConnection.invoke('SendMessage', message.nick, message.content).then(function () {
      console.log('Invocation of NewContosoChatMessage succeeded');
    }).catch(function (error) {
      console.log('Invocation of NewContosoChatMessage failed. Error: ' + error);
    });
  }

}










