import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { WiadomoscComponent } from './wiadomosc/wiadomosc.component';
import { PisanieWiadomosciComponent } from './pisanie-wiadomosci/pisanie-wiadomosci.component';

@NgModule({
  declarations: [
    AppComponent,
    WiadomoscComponent,
    PisanieWiadomosciComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
