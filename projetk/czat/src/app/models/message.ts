export interface messageSend {
  nick: string;
  content: string;
}
export interface messageShow {
  nick: string;
  content: string;
  date: string;
}

