import { Component, EventEmitter, Output, OnInit, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { messageSend } from '../../app/models/message';

@Component({
  selector: 'app-pisanie-wiadomosci',
  templateUrl: './pisanie-wiadomosci.component.html',
  styleUrls: ['./pisanie-wiadomosci.component.sass']
})
export class PisanieWiadomosciComponent implements OnInit {

  name = new FormControl('');
  message = new FormControl('');
  @Output() writer = new EventEmitter<string>();
  @Output() sendMessage = new EventEmitter<messageSend>();
  constructor() { }

  ngOnInit(): void {
  }

  send() {
    const data: messageSend = {
      nick: this.name.value,
      content: this.message.value
    };
    this.sendMessage.emit(data);
    this.message.setValue('');
  }
  pickWriter(date: any) {
    this.writer.emit(date.trim());
  }


  get valid() {
    if (this.name.value.trim() !== '' && this.message.value.trim() !== '') {
      return null
    } else {
      return true
    }
  }

}
