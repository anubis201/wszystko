import { Component, OnInit, Input } from '@angular/core';
import { messageShow } from '../../app/models/message';

@Component({
  selector: 'app-wiadomosc',
  templateUrl: './wiadomosc.component.html',
  styleUrls: ['./wiadomosc.component.sass']
})
export class WiadomoscComponent implements OnInit {
  @Input() messages: messageShow[] = [];
  @Input() writer!: string;

  constructor() { }

  ngOnInit(): void {
  }

  }
